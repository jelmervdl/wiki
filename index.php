<?php

require 'etc/config.php';

require 'include/session.php';
require 'include/wiki.php';
require 'include/parsedown.php';
require 'include/template.php';

if (empty($_GET['slug']))
	$_GET['slug'] = 'index';

switch ($_GET['slug'])
{
	case ':login':
		if ($_SERVER['REQUEST_METHOD'] == 'POST') {
			if (cover_session_create($_POST['email'], $_POST['password'], $error))
				header('Location: ' . (!empty($_POST['slug']) ? link_page($_POST['slug']) : link_page('index')));
			else
				view_unauthorized(!empty($_POST['slug']) ? $_POST['slug'] : null, $error);
		}
		else view_unauthorized();
		break;

	case ':logout':
		cover_session_destroy();
		header('Location: /');
		break;

	default:
		$page = WikiPage::getBySlug($pdo, $_GET['slug']);

		if (isset($_GET['mode']) && $_GET['mode'] == 'edit')
			edit_page($page);
		else
			view_page($page);
		
		break;
}

function view_unauthorized($slug = null, $error = null)
{
	$title = 'Log in';

	$content = render_template('tpl/login.phtml', compact('slug', 'error'));

	echo render_template('tpl/layout.phtml', compact('title', 'content'));
}

function view_page(WikiPage $page)
{
	$parsedown = new Parsedown();
	$content = $parsedown->parse($page->content);

	$title = preg_match('{<h1>(.+?)</h1>}', $content, $match)
		? strip_tags($match[1])
		: $page->slug;

	$content = render_template('tpl/page.phtml', compact('page', 'content'));

	echo render_template('tpl/layout.phtml', compact('title', 'content'));
}