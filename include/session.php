<?php

defined('COVER_SESSION_COOKIE_NAME') || define('COVER_SESSION_COOKIE_NAME', 'cover_app_session_id');

function http_json_request($url, array $data = null)
{
	$options = array(
	    'http' => is_array($data)
	    	? array(
				'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
				'method'  => 'POST',
				'content' => http_build_query($data))
		    : array(
		    	'method'  => 'GET')
	);

	$context  = stream_context_create($options);

	$response = file_get_contents($url, false, $context);

	if (!$response)
		throw new Exception('No response');

	$data = json_decode($response);

	if ($data === null)
		throw new Exception('Response could not be parsed as JSON: <pre>' . htmlentities($response) . '</pre>');

	return $data;
}

function get_cover_session()
{
	static $session = null;

	// Is there a local session id?
	if (!empty($_COOKIE[COVER_SESSION_COOKIE_NAME]))
		$session_id = $_COOKIE[COVER_SESSION_COOKIE_NAME];

	// Is there a cover website global session id available?
	elseif (!empty($_COOKIE['cover_session_id']))
		$session_id = $_COOKIE['cover_session_id'];

	// If both not, bail out. I have no place else to look :(
	else
		return false;

	if ($session !== null)
		return $session;

	$data = array('session_id' => $session_id);

	$response = http_json_request('https://www.svcover.nl/api.php?method=session_get_member', $data);

	if (!$response->result)
		$session = false;

	return $session = $response->result;
}

function cover_session_logged_in()
{
	return get_cover_session() !== false;
}

function cover_session_create($email, $password, $application, &$error = null)
{
	$data = compact('email', 'password', 'application');

	$response = http_json_request('https://www.svcover.nl/api.php?method=session_create', $data);

	if ($response->result) {
		setcookie(COVER_SESSION_COOKIE_NAME, $response->result->session_id, time() + 7 * 24 * 3600, '/');
		return true;
	}
	else {
		$error = $response->error;
		return false;
	}
}

function cover_session_destroy()
{
	if (!isset($_COOKIE[COVER_SESSION_COOKIE_NAME]))
		return null;

	$data = array('session_id' => $_COOKIE[COVER_SESSION_COOKIE_NAME]);

	$response = http_json_request('https://www.svcover.nl/api.php?method=session_destroy', $data);

	setcookie(COVER_SESSION_COOKIE_NAME, '', time() - 24 * 3600, '/');
}

function cover_session_status()
{
	$session = get_cover_session();

	if (!$session)
		$content = '<a id="login-link" class="button" href="/:login">Log in</a>';
	else
		$content = sprintf('Logged in as %s.
			<a class="button" href="%s">Log out</a>',
			$session->voornaam, 
			!empty($_COOKIE[COVER_SESSION_COOKIE_NAME])
				? '/:logout'
				: 'https://www.svcover.nl/dologout.php?referrer=' . rawurlencode('https://sd.svcover.nl/')
			);

	return sprintf('<div class="session">%s</div>', $content);
}

function cover_session_user_in_committee($committee)
{
	$session = get_cover_session();

	if (!$session)
		return array();

	$response = http_json_request('https://www.svcover.nl/api.php?method=get_committees&member_id=' . $session->id);

	return $response->result;
}