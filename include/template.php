<?php

function render_template()
{
	extract(func_get_arg(1));
	ob_start();
	include func_get_arg(0);
	return ob_get_clean();
}

function link_page($slug)
{
	return 'index.php?slug=' . rawurlencode($slug);
}

function link_edit_page($slug)
{
	return 'index.php?mode=edit&slug=' . rawurlencode($slug);
}

function esc_html($html)
{
	return htmlspecialchars($html, ENT_COMPAT, 'utf-8');
}

function esc_attr($html)
{
	return htmlspecialchars($html, ENT_QUOTES, 'utf-8');
}
