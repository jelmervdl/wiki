<?php

function human_implode($values)
{
	if (count($values) === 0)
		return '';

	if (count($values) === 1)
		return $values[0];

	$first_elements = array_slice($values, 0, -1);

	$last_element = end($values);

	return implode(' and ', array(
		implode(', ', $first_elements),
		$last_element));
}
