<?php

require_once 'include/human.php';
require_once 'include/dataobject.php';

class WikiNamespace extends DataObject
{
	public $id;

	public $slug;

	public $name;

	public function save()
	{
		$data = array(
			':slug' => $this->_generateSlug($this->_name),
			':name' => $this->name
		);

		if (empty($this->id)) {
			$stmt = $this->_pdo->prepare("INSERT INTO namespaces (slug, name) VALUES (:slug, :name)");
		}
		else {
			$stmt = $this->_pdo->prepare("UPDATE namespaces SET slug = :slug, name = :name WHERE id = :id");
			$data[':id'] = 'id';
		}

		$stmt->execute($data);

		if (empty($this->id))
			$this->id = $this->_pdo->lastInsertId();
	}

	public function currentUserMayRead()
	{
		return cover_session_user_in_committee($this->slug);
	}

	public function currentUserMayWrite()
	{
		return cover_session_user_in_committee($this->slug);
	}

	/* Static access methods */

	static public function getById(PDO $pdo, $id)
	{
		return self::_querySingle($pdo, compact('id'));
	}

	static public function getBySlug(PDO $pdo, $slug)
	{
		return self::_querySingle($pdo, compact('slug'));
	}

	/* Implementing DataObject interface */

	static protected function _prepareStmt(PDO $pdo, array $sql_conditions)
	{
		return $pdo->prepare("
			SELECT
				n.id,
				n.slug,
				n.name
			FROM
				namespaces n
			WHERE
				" . implode(" AND ", $sql_conditions));
	} 
}

class WikiPage extends DataObject
{
	const ACCESS_MEMBER = 2;

	const ACCESS_EVERYONE = 1;

	const ACCESS_NAMESPACE = 0;

	public $id;

	public $version_id;

	public $slug;

	public $namespace;

	public $readable = self::ACCESS_EVERYONE;

	public $editable = self::ACCESS_NAMESPACE;

	public $author_id;

	public $edited_on;

	public $content;

	public function save()
	{
		$this->_updatePage();

		$this->_insertRevision();
	}	

	protected function _updatePage()
	{
		$data = array(
			':slug' => $this->slug,
			':namespace' => $this->namespace,
			':editable' => $this->editable,
			':readable' => $this->readable
		);

		if (empty($this->id))
			$stmt = $this->_pdo->prepare("INSERT INTO pages (slug, namespace, editable, readable) VALUES(:slug, :namespace, :editable, :readable)");
		else {
			$stmt = $this->_pdo->prepare("UPDATE pages SET slug = :slug, namespace = :namespace, editable = :editable, readable = :readable WHERE id = :id");
			$data[':id'] = $this->id;
		}

		$stmt->execute($data);

		if (empty($this->id))
			$this->id = $this->_pdo->lastInsertId();
	}

	protected function _insertRevision()
	{
		$data = array(
			':page_id' => $this->id,
			':content' => $this->content,
			':author_id' => $this->author_id
		);

		$stmt = $this->_pdo->prepare("INSERT INTO versions (page_id, content, author_id, created_on) VALUES (:page_id, :content, :author_id, NOW())");

		$stmt->execute($data);

		$this->version_id = $this->_pdo->lastInsertId();
	}

	public function currentUserMayRead()
	{
		switch ($this->readable)
		{
			case self::ACCESS_EVERYONE:
				return true;

			case self::ACCESS_MEMBER:
				return (bool) get_cover_session();

			case self::ACCESS_NAMESPACE:
				return $this->namespace()->currentUserMayRead();

			default:
				throw new Exception("Unknown page.readable status for wiki page");
		}
	}

	public function currentUserMayWrite()
	{
		switch ($this->writable)
		{
			case self::ACCESS_EVERYONE:
				return true;

			case self::ACCESS_MEMBER:
				return (bool) get_cover_session();

			case self::ACCESS_NAMESPACE:
				return $this->namespace()->currentUserMayWrite();

			default:
				throw new Exception("Unknown page.writable status for wiki page");
		}
	}

	/* Static access methods */

	static public function getBySlug(PDO $pdo, $slug)
	{
		return self::_querySingle($pdo, compact('slug'));
	}

	static protected function _prepareStmt(PDO $pdo, array $sql_conditions)
	{
		return $pdo->prepare("
			SELECT
				p.id,
				p.slug,
				p.namespace,
				p.readable,
				p.editable,
				v.id as version_id,
				v.author_id,
				v.edited_on,
				v.content
			FROM
				pages p
			RIGHT JOIN
				page_versions v ON
				v.page_id = p.id
			LEFT JOIN -- Try to find a version that is newer than the selected one
				page_versions a ON
				a.page_id = p.id
				AND a.edited_on > v.edited_on
			WHERE
				" . implode(" AND ", $sql_conditions) . "
				AND a.id IS NULL -- only if there is no more up to date version of the content
			GROUP BY
				p.id,
				p.slug,
				p.namespace,
				p.readable,
				p.editable,
				v.id,
				v.author_id,
				v.edited_on,
				v.content");
	}
}
