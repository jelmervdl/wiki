<?php

require_once 'include/human.php';

abstract class DataObject
{
	protected $_pdo;

	public function __construct(PDO $pdo)
	{
		$this->_pdo = $pdo;
	}

	static protected function _querySingle(PDO $pdo, array $conditions)
	{
		$stmt = self::_query($pdo, $conditions);

		$stmt->execute();

		if (!($namespace = $stmt->fetch()))
			throw new DataObjectNotFoundException($conditions);

		return $namespace;
	}

	static protected function _query(PDO $pdo, array $conditions)
	{
		$sql_conditions = array_map(function($column) {
			return sprintf('%s = :%1$s', $column);
		}, array_keys($conditions));

		$stmt = static::_prepareStmt($pdo, $sql_conditions);

		foreach ($conditions as $column => $value)
			$stmt->bindValue(':' . $column, $value);

		$stmt->setFetchMode(PDO::FETCH_CLASS, get_called_class(), array($pdo));

		return $stmt;
	}

	// abstract static protected function _prepareStmt(PDO $pdo, array $sql_conditions);
}

class DataObjectNotFoundException extends Exception
{
	public function __construct($conditions)
	{
		foreach ($conditions as $name => $value)
			$conditions[$name] = sprintf('%s "%s"', $name, $value);

		parent::__construct("Could not find namespace with " . human_implode($conditions));
	}
}